﻿using MatrixProcessor.IO.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace MatrixProcessor.App
{
    class Program
    {
        public static string[] Arguments { get; private set; }

        static void Main(string[] args)
        {
            Arguments = args;

            if (args.Length < 1)
            {
                Usage();
                return;
            }

            var directoryProcessor = Startup.Container.GetRequiredService<IDirectoryProcessor>();
            directoryProcessor.Progress += (current, count) => Console.Write($"\rОбработано {current}/{count} файлов.\t\t");
            directoryProcessor.Process(args[0]);

            Console.WriteLine();
        }

        static void Usage() => Console.WriteLine("Запуск: dotnet MatrixProcessor.App.dll <входная_директория>");
    }
}
