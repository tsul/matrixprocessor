﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.App
{
    /// <summary>
    /// Методы расширения интерфейса <see cref="IServiceCollection"/>.
    /// </summary>
    public static class IServiceCollectionExtensions
    {
        /// <summary>
        /// Добавить настройку к коллекции сервисов.
        /// </summary>
        /// <typeparam name="TSettings">Тип настройки.</typeparam>
        /// <param name="services">Коллекция описателей сервисов.</param>
        /// <param name="defatulSettings">Значение настройки по умолчанию.</param>
        /// <param name="configuration">Конфигурация приложения.</param>
        /// <param name="section">Имя раздела конфигурации (по умолчанию - имя типа <typeparamref name="TSettings"/>)</param>
        /// <returns>Та же коллекция описателей сервисов <paramref name="services"/>.</returns>
        public static IServiceCollection AddSettings<TSettings>(this IServiceCollection services, TSettings defatulSettings, IConfiguration configuration, string section = null)
            where TSettings : class
        {
            services.AddSingleton(sp =>
            {
                var settings = defatulSettings;
                configuration.GetSection(section ?? typeof(TSettings).Name).Bind(settings);
                return settings;
            });
            return services;
        }

        /// <summary>
        /// Зарегистрировать в коллекции сервисов экземпляр класса настроек <typeparamref name="TSettings"/>, заполненный согласно конфигурации <paramref name="configuration"/>
        /// </summary>
        /// <param name="services">Коллекция описателей сервисов.</param>
        /// <param name="configuration">Конфигурация приложения.</param>
        /// <param name="section">Имя секции конфигурации (по умолчанию - имя типа <typeparamref name="TSettings"/>)</param>
        public static IServiceCollection AddSettings<TSettings>(this IServiceCollection services, IConfiguration configuration, string section = null)
            where TSettings : class, new()
        {
            return services.AddSettings(new TSettings(), configuration, section);
        }
    }
}
