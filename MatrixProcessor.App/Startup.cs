﻿using Autofac;
using Autofac.Configuration;
using Autofac.Extensions.DependencyInjection;
using MatrixProcessor.Engine.Contracts;
using MatrixProcessor.IO;
using MatrixProcessor.IO.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.App
{
    static class Startup
    {
        private static Lazy<IServiceProvider> _container = new Lazy<IServiceProvider>(ConfigureContainer);
        private static Lazy<IConfiguration> _configuration = new Lazy<IConfiguration>(LoadConfiguration);
        public static IServiceProvider Container => _container.Value;
        public static IConfiguration Configuration => _configuration.Value;

        private static IConfiguration LoadConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddYamlFile("appsettings.yaml", optional: true, reloadOnChange: true)
                .AddYamlFile("components.yaml")
                //.AddCommandLine(Program.Arguments)
                ;

            return builder.Build();
        }

        private static IServiceProvider ConfigureContainer()
        {
            var services = new ServiceCollection();

            // Настройки
            services
                .AddSettings(DirectoryProcessorConfig.Default, Configuration)
                .AddSettings(FileProcessorConfig.Default, Configuration)
                .AddSettings(MatrixProcessorConfig.Default, Configuration);

            // autofac (для конфигурирования движков матричных операций через DI)
            var builder = new ContainerBuilder();

            // components.yaml
            var configurationModule = new ConfigurationModule(Configuration);
            builder.RegisterModule(configurationModule);

            // Service
            builder.RegisterType<DirectoryProcessor>().As<IDirectoryProcessor>();
            builder.RegisterType<FileProcessor>().As<IFileProcessor>();

            builder.Populate(services);
            return new AutofacServiceProvider(builder.Build());
        }
    }
}
