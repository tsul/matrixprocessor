﻿using MatrixProcessor.Engine.Contracts;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MatrixProcessor.Engine.MathNet
{
    public class MathNetMatrixProcessor : IMatrixProcessor
    {
        private readonly MatrixProcessorConfig config;

        public MathNetMatrixProcessor(MatrixProcessorConfig config)
        {
            this.config = config ?? throw new ArgumentNullException(nameof(config));
        }

        public MatrixResponse Process(MatrixRequest request)
        {
            request.Validate(config);

            var operands = request.Matrices.Select(m => DenseMatrix.OfArray(m.Array)).ToArray();
            Matrix<double> result = null;

            switch(request.Operation)
            {
                case MatrixOperation.Multiply:
                    result = operands[0];
                    foreach (var m in operands.Skip(1))
                        result *= m;
                    break;
                case MatrixOperation.Add:
                    result = operands[0];
                    foreach (var m in operands.Skip(1))
                        result += m;
                    break;
                case MatrixOperation.Substract:
                    result = operands[0];
                    foreach (var m in operands.Skip(1))
                        result -= m;
                    break;
                case MatrixOperation.Transpose:
                    result = operands[0].Transpose();
                    break;
            }

            return new MatrixResponse { Matrix = new Contracts.Matrix { Array = result.ToArray() } };
        }
    }
}
