﻿using MatrixProcessor.Engine.Contracts;
using Meta.Numerics.Matrices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MatrixProcessor.Engine.MetaNumerics
{
    public class MetaNumericsMatrixProcessor : IMatrixProcessor
    {
        private readonly MatrixProcessorConfig config;

        public MetaNumericsMatrixProcessor(MatrixProcessorConfig config)
        {
            this.config = config ?? throw new ArgumentNullException(nameof(config));
        }

        public MatrixResponse Process(MatrixRequest request)
        {
            request.Validate(config);

            var operands = request.Matrices.Select(m => new RectangularMatrix(m.Array)).ToArray();
            RectangularMatrix result = null;

            switch (request.Operation)
            {
                case MatrixOperation.Multiply:
                    result = operands[0];
                    foreach (var m in operands.Skip(1))
                        result *= m;
                    break;
                case MatrixOperation.Add:
                    result = operands[0];
                    foreach (var m in operands.Skip(1))
                        result += m;
                    break;
                case MatrixOperation.Substract:
                    result = operands[0];
                    foreach (var m in operands.Skip(1))
                        result -= m;
                    break;
                case MatrixOperation.Transpose:
                    result = operands[0].Transpose;
                    break;
            }

            return new MatrixResponse { Matrix = new Contracts.Matrix { Array = result.ToArray() } };
        }
    }
}
