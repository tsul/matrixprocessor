﻿using MatrixProcessor.Engine.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace MatrixProcessor.IO
{
    public static class MatrixExtensions
    {
        static readonly string elementsSeparator = " ";

        public static void Dump(this Matrix matrix, TextWriter textWriter, CultureInfo cultureInfo)
        {
            var a = matrix.Array;
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    textWriter.Write(a[i, j].ToString(cultureInfo));
                    textWriter.Write(elementsSeparator);
                }
                textWriter.WriteLine();
            }
        }

        public static Matrix LoadMatrix(this TextReader tr, CultureInfo cultureInfo)
        {
            var rows = new List<double[]>();
            string ss;
            while (true)
            {
                ss = tr.ReadLine();
                if (String.IsNullOrWhiteSpace(ss))
                    break;
                var nums = ss.Split(new[] { elementsSeparator }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(s => double.Parse(s, cultureInfo)).ToArray();
                rows.Add(nums);
            }
            if (rows.Count == 0)
                return null;
            if (rows.Select(nn => nn.Length).Distinct().Count() != 1)
                throw new FormatException("Different count of numbers in the rows.");

            int n = rows.Count;
            int m = rows[0].Length;
            double[,] a = new double[n, m];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    a[i, j] = rows[i][j];
            return new Matrix { Array = a };
        }
    }
}
