﻿using MatrixProcessor.IO.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MatrixProcessor.IO
{
    public class DirectoryProcessor : IDirectoryProcessor
    {
        private readonly DirectoryProcessorConfig config;
        private readonly IFileProcessor fileProcessor;

        static readonly string resultSuffix = "_result";

        public DirectoryProcessor(DirectoryProcessorConfig config, IFileProcessor fileProcessor)
        {
            this.config = config ?? throw new ArgumentNullException(nameof(config));
            this.fileProcessor = fileProcessor ?? throw new ArgumentNullException(nameof(fileProcessor));
        }

        public event Action<int, int> Progress;

        public void Process(string directory)
        {
            var searchOptions = config.Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

            var filesQuery = config.InputFileMasks
                .SelectMany(fileMask => Directory.EnumerateFiles(directory, fileMask, searchOptions))
                .Where(name => !Path.GetFileNameWithoutExtension(name).EndsWith(resultSuffix));

            var files = filesQuery.ToList();
            int processed = 0;

            foreach (string inputPath in files)
            {
                string outputPath = MakeOutputPath(inputPath);
                fileProcessor.Process(inputPath, outputPath);
                processed++;
                Progress?.Invoke(processed, files.Count);
            }
        }

        static string MakeOutputPath(string inputPath)
        {
            string dirName = Path.GetDirectoryName(inputPath);
            string fileName = Path.GetFileNameWithoutExtension(inputPath);
            string extension = Path.GetExtension(inputPath);
            if (extension == "")
                extension = null;

            return Path.ChangeExtension(Path.Combine(dirName, fileName + resultSuffix), extension);
        }
    }
}
