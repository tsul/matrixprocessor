﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.IO
{
    public class DirectoryProcessorConfig
    {
        public bool Recursive { get; set; }
        public string[] InputFileMasks { get; set; }


        public static DirectoryProcessorConfig Default
            =>
            new DirectoryProcessorConfig
            {
                Recursive = false,
                InputFileMasks = new string[0]  // не пустой плохо: мёрж конфига добавляет элементы, не очищая исходные.
            };
    }
}
