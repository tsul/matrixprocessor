﻿using MatrixProcessor.Engine.Contracts;
using MatrixProcessor.IO.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace MatrixProcessor.IO
{
    public class FileProcessor : IFileProcessor
    {
        private readonly FileProcessorConfig config;
        private readonly IMatrixProcessor matrixProcessor;

        public FileProcessor(FileProcessorConfig config, IMatrixProcessor matrixProcessor)
        {
            this.config = config ?? throw new ArgumentNullException(nameof(config));
            this.matrixProcessor = matrixProcessor ?? throw new ArgumentNullException(nameof(matrixProcessor));
        }

        public void Process(string inFilePath, string outFilePath)
        {
            var culture = CultureInfo.CreateSpecificCulture(config.NumbersCultureName);
            var requests = new List<MatrixRequest>();

            using (var sr = new StreamReader(inFilePath))
            {
                string op = sr.ReadLine();
                var mop = (MatrixOperation)Enum.Parse(typeof(MatrixOperation), op, true);
                if (mop == MatrixOperation.Undefined || !Enum.IsDefined(typeof(MatrixOperation), mop))
                    throw new FormatException("Unknown operation");

                if (sr.ReadLine() != "")
                    throw new FormatException("Second line is not empty");

                var matrices = new List<Matrix>();
                Matrix m;
                while ((m = sr.LoadMatrix(culture)) != null)
                    matrices.Add(m);

                if (mop == MatrixOperation.Transpose)
                {
                    foreach (var matrix in matrices)
                        requests.Add(new MatrixRequest { Operation = mop, Matrices = new[] { matrix } });
                }
                else
                    requests.Add(new MatrixRequest { Operation = mop, Matrices = matrices.ToArray() });
            }

            using (var sw = new StreamWriter(outFilePath))
            {
                foreach (var rq in requests)
                {
                    var response = matrixProcessor.Process(rq);
                    response.Matrix.Dump(sw, culture);
                    sw.WriteLine();
                }
            }
        }
    }
}
