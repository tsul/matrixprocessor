﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.IO
{
    public class FileProcessorConfig
    {
        public string NumbersCultureName { get; set; }

        public static FileProcessorConfig Default
            =>
            new FileProcessorConfig
            {
                NumbersCultureName = ""     // invariant culture
            };
    }
}
