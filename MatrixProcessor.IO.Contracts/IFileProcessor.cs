﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.IO.Contracts
{
    public interface IFileProcessor
    {
        void Process(string inFilePath, string outFilePath);
    }
}
