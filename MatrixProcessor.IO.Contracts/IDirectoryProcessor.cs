﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.IO.Contracts
{
    public interface IDirectoryProcessor
    {
        event Action<int, int> Progress;

        void Process(string directory);
    }
}
