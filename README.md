# README #

### What is this repository for? ###

* Test task for LECTA
* 0.1

### How do I get set up? ###

* Build and run the console MatrixProcessor.App
* Configuration
    * components.yaml -- allow to choose a math matrix library to use;
    * appsettings.yaml -- various processing settings;
* Dependencies
    * Autofac
    * Meta.Numerics
    * Math.NET Numerics
    * Microsoft.Extensions.*
* How to run tests
    * TestData
