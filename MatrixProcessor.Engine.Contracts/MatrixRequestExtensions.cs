﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.Engine.Contracts
{
    public static class MatrixRequestExtensions
    {
        // за вызов валидации отвечают имплементации
        public static void Validate(this MatrixRequest request, MatrixProcessorConfig config)
        {
            switch (request.Operation)
            {
                case MatrixOperation.Multiply:
                case MatrixOperation.Add:
                case MatrixOperation.Subtract:
                    if (request.Matrices.Length < 2)
                        throw new ArgumentOutOfRangeException(nameof(request.Matrices), "Для бинарных операций кол-во матриц-операндов не должно быть меньше двух.");
                    if (!config.AllowMoreThanTwoOperandsForBinaryOperations && request.Matrices.Length > 2)
                        throw new ArgumentOutOfRangeException(nameof(request.Matrices), "Для бинарных операций кол-во матриц-операндов не должно быть больше двух.");
                    break;
                case MatrixOperation.Transpose:
                    if (request.Matrices.Length != 1)
                        throw new ArgumentOutOfRangeException(nameof(request.Matrices), "Для операции транспонирования кол-во матриц-операндов должно быть равно одной.");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(request.Operation), $"Неизвестный тип матричной операции {request.Operation}.");
            }
        }
    }
}
