﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.Engine.Contracts
{
    public class Matrix
    {
        public double[,] Array { get; set; }
    }
}
