﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.Engine.Contracts
{
    public class MatrixResponse
    {
        public Matrix Matrix { get; set; }
    }
}
