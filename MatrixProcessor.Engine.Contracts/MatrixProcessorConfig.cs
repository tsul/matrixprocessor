﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.Engine.Contracts
{
    public class MatrixProcessorConfig
    {
        public bool AllowMoreThanTwoOperandsForBinaryOperations { get; set; }

        public static MatrixProcessorConfig Default
            =>
            new MatrixProcessorConfig
            {
                AllowMoreThanTwoOperandsForBinaryOperations = false
            };
    }
}
