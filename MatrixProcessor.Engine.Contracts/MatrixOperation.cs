﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.Engine.Contracts
{
    public enum MatrixOperation
    {
        Undefined = 0,
        Multiply = 1,
        Add = 2,
        Subtract = 3,
        Substract = 3,
        Transpose = 4
    }
}
