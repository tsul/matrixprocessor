﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.Engine.Contracts
{
    public interface IMatrixProcessor
    {
        MatrixResponse Process(MatrixRequest request);
    }
}
