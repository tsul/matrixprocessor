﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixProcessor.Engine.Contracts
{
    public class MatrixRequest
    {
        public MatrixOperation Operation { get; set; }

        public Matrix[] Matrices { get; set; }
    }
}
